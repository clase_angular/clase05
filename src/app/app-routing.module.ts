import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BusquedaComponent } from './core/componentes/busqueda/busqueda.component';
import { DetalleComponent } from './core/componentes/detalle/detalle.component';
import { HomeComponent } from './core/componentes/home/home.component';

const routes: Routes = [
  {
    path: '', // inicio
    component: HomeComponent
  },
  {
    path: 'busqueda/:texto',
    component: BusquedaComponent
  },
  {
    path: 'detalle/:id',
    component: DetalleComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
