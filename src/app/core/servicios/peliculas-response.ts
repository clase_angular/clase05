export interface PeliculasResponse {
  query: string;
  results?: (PeliculaEntity)[] | null;
}

export interface PeliculaEntity {
  id: string;
  image?: Image | null;
  title?: string | null;
  titleType?: string | null;
  year?: number | null;
  principals?: (PrincipalsEntity)[] | null;
  akas?: (string)[] | null;
  name?: string | null;
}

export interface Image {
  height: number;
  id: string;
  url: string;
  width: number;
}

export interface PrincipalsEntity {
  id: string;
  name: string;
  roles?: (RolesEntity)[] | null;
}

export interface RolesEntity {
  character: string;
  characterId?: string | null;
}
