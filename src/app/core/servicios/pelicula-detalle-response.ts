export interface PeliculaDetalleResponse {
  id: string;
  title: Title;
  genres?: (string)[] | null;
  releaseDate: string;
  plotOutline: PlotOutline;
}
export interface Title {
  id: string;
  image: Image;
  title: string;
  titleType: string;
  year: number;
}
export interface Image {
  height: number;
  id: string;
  url: string;
  width: number;
}
export interface PlotOutline {
  id: string;
  text: string;
}
