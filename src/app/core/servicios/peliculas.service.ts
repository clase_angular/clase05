import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PeliculaDetalleResponse } from './pelicula-detalle-response';
import { PeliculasResponse } from './peliculas-response';

@Injectable({
  providedIn: 'root'
})
export class PeliculasService {

  private base_url: string = "https://imdb8.p.rapidapi.com/title";
  private key: string = "4d6a0ed37emshf552b8fbd5f20e6p1f4ec5jsn28a5f5927766";

  constructor(private httpClient: HttpClient) { }

  buscarPeliculas(nombre: string): Observable<PeliculasResponse> {
    let url: string = `${this.base_url}/find`;
    let header: HttpHeaders = new HttpHeaders({
      'x-rapidapi-host': 'imdb8.p.rapidapi.com',
      'x-rapidapi-key': this.key
    });
    let params: HttpParams = new HttpParams({
      fromObject: {q: nombre}
    });

    return this.httpClient.get<PeliculasResponse>(url, {
      headers: header,
      params: params
    });
  }

  verDetallePelicula(id: string): Observable<PeliculaDetalleResponse> {
    let url: string = `${this.base_url}/get-overview-details`;
    let header: HttpHeaders = new HttpHeaders({
      'x-rapidapi-host': 'imdb8.p.rapidapi.com',
      'x-rapidapi-key': this.key
    });
    let params: HttpParams = new HttpParams({
      fromObject: {tconst: id, currentCountry: 'US'}
    });
    return this.httpClient.get<PeliculaDetalleResponse>(url, {
      headers: header,
      params: params
    })
  }

}
