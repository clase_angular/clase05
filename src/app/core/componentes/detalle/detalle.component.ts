import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PeliculaDetalleResponse } from '../../servicios/pelicula-detalle-response';
import { PeliculasService } from '../../servicios/peliculas.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss']
})
export class DetalleComponent implements OnInit {

  id: string = "";
  detalle?: PeliculaDetalleResponse | null = null;

  constructor(private route: ActivatedRoute,
              private peliculaSrv: PeliculasService) {

  }

  ngOnInit(): void {
    this.route.params.subscribe((param: Params) => {
      this.id = param["id"];
      this.buscarPeliculaPorId(this.id);
    });
  }

  buscarPeliculaPorId(id: string) {
    this.peliculaSrv.verDetallePelicula(id)
    .subscribe(
      (response: PeliculaDetalleResponse) => {
        this.detalle = response;
      },
      (error) => {}
    );
  }

}
