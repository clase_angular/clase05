import { ANALYZE_FOR_ENTRY_COMPONENTS, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PeliculaEntity } from '../../servicios/peliculas-response';

@Component({
  selector: 'app-item-pelicula',
  templateUrl: './item-pelicula.component.html',
  styleUrls: ['./item-pelicula.component.scss']
})
export class ItemPeliculaComponent implements OnInit {

  @Input() pelicula?: PeliculaEntity | null = null; // propiedades
  @Output() onTap: EventEmitter<string> = new EventEmitter<string>();// eventos

  constructor() { }

  ngOnInit(): void {
  }

  verDetalle() {
    let fullId = this.pelicula?.id.split("/");
    let id = fullId![2];
    this.onTap.emit(id);
  }

}
