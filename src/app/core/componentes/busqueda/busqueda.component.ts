import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PeliculaEntity, PeliculasResponse } from '../../servicios/peliculas-response';
import { PeliculasService } from '../../servicios/peliculas.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.scss']
})
export class BusquedaComponent implements OnInit {

  busqueda: string = "";
  listaPeliculas: Array<PeliculaEntity> = [];


  constructor(private route: ActivatedRoute,
              private router: Router,
              private peliculasSrv: PeliculasService) { }

  ngOnInit(): void {
    this.route.params.subscribe((param: Params) => {
      this.busqueda = param['texto'];
      this.buscarPeliculasPorNombre(this.busqueda);
    });
  }

  buscarPeliculasPorNombre(nombre: string){
    this.peliculasSrv.buscarPeliculas(nombre)
    .subscribe(
      (response: PeliculasResponse) => {
        if (response.results?.length) {
          this.listaPeliculas = response.results.filter(x => x.titleType == "movie");
        }
      },
      (error) => {}
    );
  }

  verDetallePelicula(id: string) {
    this.router.navigate(['detalle', id], {state: {nombre: 'Yrving', apellido: 'Molina'}});
  }

}
