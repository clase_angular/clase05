import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  texto: string = "";

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  buscarPeliculas() {
    this.router.navigate(['busqueda', this.texto]);
    this.texto = "";
  }

}
